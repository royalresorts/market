import React from "react";
import { useParams } from "react-router-dom";

import DocumentTitle from "react-document-title";
import GenericSection from "../components/genericsection";
import Slide from "../components/slide";
import getTexto from "../libs/messages";
import UnitHandler from "../components/unitHandler_offer";
import Related from "../components/relatedproducts";
import { getCurrency } from "../libs/language";
import {
  RetrieveRandomObjByCat,
  getRichText,
  getOfferPriceDetails,
} from "../libs/helpers";
import { LazyLoadImage } from "react-lazy-load-image-component";
import _ from "lodash";
import "../../scss/components/itemDetail.scss";
import { Helmet } from "react-helmet";

export default function Detail({ items, site }) {
  let params = useParams();
  let itemSelected = null;
  let gallery = [];
  let renderSubItems = "";
  let priceInfOffer = {};
  let itemsRandomByCat = new Array();
  // let count = 0;

  //this code is optimized to do not iterate across all of items
  let resultItemSelected = items.filter(
    (i) => parseInt(i.ID) === parseInt(params.producto)
  );

  if (resultItemSelected.length) itemSelected = resultItemSelected[0];

  if (resultItemSelected.length) {
    if (itemSelected.ItemPhoto.length) {
      let itemsGallery = itemSelected.ItemPhoto;
      itemsGallery.map((gal) => {
        gallery.push({ img: gal.ItemPhoto });
      });
    } else {
      gallery = [
        {
          img: "/img/default.jpg",
        },
      ];
    }
    priceInfOffer = getOfferPriceDetails(itemSelected);
    itemSelected["priceInfOffer"] = priceInfOffer;
    renderSubItems = itemSelected.OfferDetail.map((element) => (
      <>
        {`- ${element.ItemDesc}`} <br></br>
      </>
    ));
  }

  return (
    <DocumentTitle
      title={
        itemSelected
          ? itemSelected.Description != ""
            ? itemSelected.Description
            : itemSelected.OfferCode.toLowerCase()
          : "Item Description"
      }
    >
      {resultItemSelected.length ? (
        <div id="root">
          <Helmet>
            ‍
            <title>
              Royal Market -{" "}
              {itemSelected.OfferCode != ""
                ? itemSelected.OfferCode.toLowerCase()
                : itemSelected.Description.toLowerCase()}
            </title>
            ‍
            <meta
              name="description"
              content={itemSelected.Description.toLowerCase()}
            />
          </Helmet>
          <div className="App">
            <div className="main itemDetail">
              <div className="main_container">
                <GenericSection className="row">
                  <div className="section x8" style={{ display: "block" }}>
                    <div className="slidecontainer">
                      <Slide slides={gallery} width="200px" showTumbs={true} />
                    </div>
                  </div>
                  <div className="section x4">
                    <div className="details">
                      <h3
                        dangerouslySetInnerHTML={{
                          __html:
                            itemSelected.OfferCode != ""
                              ? getRichText(
                                  itemSelected.OfferCode.toLowerCase()
                                )
                              : getRichText(
                                  itemSelected.Description.toLowerCase()
                                ),
                        }}
                      ></h3>
                      <small>({itemSelected.OfferDetail.length}) Items</small>
                      <p>{renderSubItems}</p>
                      <hr />
                      <table width="100%">
                        <tbody>
                          <tr>
                            <td>
                              <strong>{getTexto("Unit price")}:</strong>{" "}
                            </td>
                            <td>
                              $ {itemSelected.priceInfOffer.price}{" "}
                              {getCurrency()}
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <strong>{getTexto("Code")}:</strong>
                            </td>
                            <td>{itemSelected.ID}</td>
                          </tr>
                          <tr>
                            <td>
                              <strong>{getTexto("Type")}:</strong>
                            </td>
                            <td>{itemSelected.OfferType}</td>
                          </tr>
                          <tr>
                            <td>
                              <strong>{getTexto("Min. for sale")}:</strong>
                            </td>
                            <td>{1}</td>
                          </tr>
                        </tbody>
                      </table>
                      <hr />
                      <p
                        style={{
                          margin: "0px",
                          padding: "0px",
                          paddingBottom: "10px",
                        }}
                      >
                        <strong>{getTexto("About this Item")}:</strong>
                      </p>
                      <p
                        style={{ margin: "0px", padding: "0px" }}
                        dangerouslySetInnerHTML={{
                          __html:
                            itemSelected.Description != ""
                              ? getRichText(itemSelected.Description)
                              : getRichText(
                                  itemSelected.Description.toLowerCase()
                                ),
                        }}
                      ></p>
                    </div>
                  </div>
                  <div className="section x4">
                    <div className="item_container">
                      <UnitHandler item={itemSelected} detail />
                    </div>
                    <div className="promo_container">
                      <a
                        href={site.configuration.contenido.imgOnItemDetail.url}
                      >
                        <picture loading="lazy">
                          {site.configuration.contenido.imgOnItemDetail.imgM !=
                          "" ? (
                            <source
                              media="(max-width: 767px)"
                              srcSet={`${site.configuration.contenido.imgOnItemDetail.imgM}`}
                            />
                          ) : (
                            ""
                          )}
                          <source
                            media="(min-width: 767px)"
                            srcSet={`${site.configuration.contenido.imgOnItemDetail.img}`}
                          />
                          <LazyLoadImage
                            className="img"
                            src={
                              site.configuration.contenido.imgOnItemDetail.img
                            }
                            alt="Banner On Detail"
                          />
                        </picture>

                        {/* <LazyLoadImage
                          className="img"
                          src={site.configuration.contenido.imgOnItemDetail.img}
                          alt="Image Before Footer"
                        /> */}
                      </a>
                    </div>
                  </div>
                </GenericSection>
                {/* {itemsRandomByCat.length > 0 && itemsRandomByCat.length == 3 ? (
                  <Related items={itemsRandomByCat} />
                ) : (
                  ""
                )} */}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div id="root" style={{ backgroundColor: "white" }}>
          <div className="App">
            <div className="main itemDetail">
              <div className="main_container">
                <h2
                  style={{
                    textAlign: "center",
                    color: "#535353",
                    minHeight: "400px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {getTexto("This Item doesn´t exist")}
                </h2>
              </div>
            </div>
          </div>
        </div>
      )}
    </DocumentTitle>
  );
}
