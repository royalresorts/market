import {
  SETLOADCART,
  FETCHCART,
  SETCARTITEM,
  DELETECARTITEM,
  SETITEMTOSESSION,
  OPENLOADER,
  RESETCARTITEM,
} from '../actions/cartActions';
const INITIAL_STATE = { itemsCart: [], isShowed: false, loader: false };

export default function (state = INITIAL_STATE, action) {
  let newState = state;
  switch (action.type) {
    case SETLOADCART:
      newState = { ...state, isShowed: action.payload };
      break;
    case SETCARTITEM:
      let newListItemsCart = state.itemsCart;
      let newItemToCart = action.payload.item;
      let Exist = false;

      state.itemsCart.map((item) => {
        if (item.item.SItemCode == newItemToCart.SItemCode) {
          Exist = true;
          if (action.payload.onList == true) {
            if (item.item.YnAllowFractionalSale == true) {
              item.totalItems =
                parseFloat(item.totalItems) +
                parseFloat(action.payload.totalItems);
            } else {
              item.totalItems += parseInt(action.payload.totalItems);
            }
          } else {
            item.totalItems = action.payload.totalItems;
          }
        }
      });

      if (!Exist) {
        newListItemsCart.push(action.payload);
      }

      localStorage.setItem(
        'cart',
        window.btoa(JSON.stringify(newListItemsCart))
      );
      newState = {
        ...state,
        itemsCart: newListItemsCart,
        itemsCount: newListItemsCart.length,
        ...getDetailsCart(newListItemsCart),
      };
      break;

    case DELETECARTITEM:
      let codeItem = action.payload;
      let newArray = [];
      // let cant = 0;

      newArray = state.itemsCart.filter((i) => i.item.SItemCode !== codeItem);

      // state.itemsCart.map((item) => {
      //   if (item.item.SItemCode !== codeItem) {
      //     newArray.push(item);
      //     cant = cant + 1;
      //   }
      // });
      localStorage.setItem('cart', window.btoa(JSON.stringify(newArray)));
      newState = {
        ...state,
        itemsCart: newArray,
        itemsCount: newArray.length,
        ...getDetailsCart(newArray),
      };
      break;
    case RESETCARTITEM:
      localStorage.setItem('cart', window.btoa(JSON.stringify([])));
      newState = {
        ...state,
        itemsCart: [],
        itemsCount: 0,
        totalPrice: parseFloat(0.0).toFixed(1),
      };
      break;
    case FETCHCART:
      let iniConfig = action.payload;
      let count = 0;
      let itemsCart = [];

      if (localStorage.getItem('cart')) {
        itemsCart = JSON.parse(window.atob(localStorage.getItem('cart')));
        itemsCart.map((item) => {
          count = count + 1;
        });
      }
      newState = {
        ...state,
        ini: iniConfig,
        itemsCart: itemsCart,
        itemsCount: count,
        ...getDetailsCart(itemsCart),
      };
      break;
    case OPENLOADER:
      newState = { ...state, loader: action.payload };
      break;
    case SETITEMTOSESSION:
      let error = '';
      if (parseInt(action.payload) == 1) {
        // window.location = action.apiServer + "Shopping/InformationPay";
        newState = { ...state, errorOnCheckout: false, loader: false };
      } else {
        error = action.apiServer;
        newState = {
          ...state,
          errorOnCheckout: true,
          errorMessage: error,
          loader: true,
        };
      }
      // newState = { ...state, errorOnCheckout: error, loader: false };
      break;
    default:
      newState = state;
  }

  return newState;
}

function getDetailsCart(cart) {
  // priceFetch +=
  //   parseFloat(action.payload.totalItems) *
  //   parseFloat(action.payload.item.DPrice).toFixed(1);
  let result = {
    subTotal: 0.0,
    discount10: 0.0,
    discountOffer: 0.0,
    total: 0.0,
  };
  cart.map((item) => {
    if (item.item.isOffer) {
      result.subTotal +=
        parseFloat(item.item.priceDetails.priceWDiscount).toFixed(2) *
        parseFloat(item.totalItems);
      result.discountOffer +=
        parseFloat(item.item.priceDetails.totalDiscount).toFixed(2) *
        parseFloat(item.totalItems);
      result.total +=
        parseFloat(item.item.priceDetails.price).toFixed(2) *
        parseFloat(item.totalItems);
    } else {
      let priceItem =
        parseFloat(item.item.DPrice).toFixed(2) * parseFloat(item.totalItems);
      result.subTotal += priceItem;
      result.discount10 += parseFloat(priceItem).toFixed(2) * 0.1;
      result.total += priceItem * 0.9;
    }
  });
  result['totalPrice'] = result.subTotal;

  return result;
}
