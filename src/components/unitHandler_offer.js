import React, { useEffect, useState, Suspense, lazy } from "react";
import { connect } from "react-redux";
import getTexto from "../libs/messages";
import { getCurrency } from "../libs/language";
import { getOfferPriceDetails } from "../libs/helpers";
import { fetchCartItems, setItemToCart } from "../actions/cartActions";

const Notification = lazy(() =>
  import(/* webpackPrefetch: true */ "./notifications")
);

function HandlerItem({
  setItemToCart,
  item,
  detail = null,
  cartItem = null,
  isRelated = null,
}) {
  let initialSell = 1; // set the initial item's value
  item.MinSell = 1; // we overwrite the minSellVariable
  let [notificationAdd, setNotification] = useState(false);
  let [notificationUpdate, setNotificationUpdate] = useState(false);

  let priceInfOffer = getOfferPriceDetails(item);

  let cartSubtotal = cartItem
    ? parseFloat(cartItem.totalItems * priceInfOffer.price).toFixed(1)
    : priceInfOffer.price;

  let [subtotal, setSubtotal] = useState(cartSubtotal);

  useEffect(() => {
    if (cartItem) {
      if (
        parseFloat(cartSubtotal).toFixed(1) !== parseFloat(subtotal).toFixed(1)
      ) {
        updateItemToCart();
      }
    }
  });
  let [unit, setUnit] = useState(
    cartItem ? cartItem.totalItems : initialSell // because is an offer we set it at 1
  );
  useEffect(() => {
    getSubTotal();
  });

  let [error, setError] = useState(false);
  let addRemoveUnit = (operation) => {
    let addNumber, removeNumber;
    let value = unit;
    addNumber = unit + parseInt(initialSell);

    if (parseFloat(unit) > 0) {
      removeNumber = unit - parseInt(initialSell);
    } else {
      removeNumber = 0;
    }
    if (operation == "+") {
      setUnit(addNumber);
    } else if (operation == "-") {
      removeNumber == 0 ? setUnit(initialSell) : setUnit(removeNumber);
    }
  };
  let addUnit = (unit) => {
    let newValor;
    newValor = parseInt(unit.target.value);

    let that = this;
    if (
      unit.target.value == null ||
      unit.target.value == "" ||
      parseFloat(unit.target.value) <= 0
    ) {
      setUnit(parseInt(initialSell));
    } else {
      setUnit(newValor);
    }
    setError(false);
  };
  let onChangeInput = (e) => {
    let Re = /^([0-9])*$/;
    let myArray = Re.exec(e.target.value);
    if (myArray != null && parseInt(myArray) % parseInt(initialSell) == 0) {
      setUnit(parseInt(e.target.value));
      setError(false);
    } else {
      setUnit("");
      setError(true);
    }
  };

  let getSubTotal = () => {
    let newSubtotal = parseInt(initialSell) * priceInfOffer.price;
    newSubtotal = parseFloat(unit * priceInfOffer.price).toFixed(2);

    setSubtotal(newSubtotal);
  };
  let getSubTotalWOffer = () => {
    let newSubtotal = parseInt(initialSell) * priceInfOffer.priceWDiscount;
    newSubtotal = parseFloat(unit * priceInfOffer.priceWDiscount).toFixed(2);

    setSubtotal(newSubtotal);
  };
  let renderInput = (
    <div className="wrap-units">
      <a
        onClick={(e) => {
          e.preventDefault();
          addRemoveUnit("-");
        }}
        href="#"
        className="btn remove"
      >
        -
      </a>
      <input
        onChange={onChangeInput.bind(this)}
        type="text"
        value={unit}
        className="input-field"
        onFocus={(i) => {
          i.target.value = "";
          setUnit("");
        }}
        onBlur={addUnit.bind(this)}
      />
      <a
        onClick={(e) => {
          e.preventDefault();
          addRemoveUnit("+");
        }}
        href="#"
        className="btn add"
        value=""
      >
        +
      </a>
    </div>
  );

  let addToCart = (o) => {
    o.preventDefault();
    let itemObject = {
      totalItems: unit,
      item: {
        ...item,
        YnAllowFractionalSale: false,
        SItemCode: item.ID,
        DPrice: priceInfOffer.price,
        priceDetails: priceInfOffer,
        isOffer: true,
      },
      onList: true,
    };
    setItemToCart(itemObject);
    setNotification(true);
    // NotificationManager.info(item.SItemName, "item added to cart", 6000);
    setUnit(parseInt(initialSell));
  };
  let updateItemToCart = () => {
    if (typeof unit === "number" || (!Number.isNaN(unit) && unit != "")) {
      let itemObject = {
        totalItems: unit,
        item: {
          ...item,
          YnAllowFractionalSale: false,
          SItemCode: item.ID,
          DPrice: priceInfOffer.price,
          priceDetails: priceInfOffer,
          isOffer: true,
        },
        onList: false,
      };
      setItemToCart(itemObject);
      setNotification(true);
      setNotificationUpdate(true);
    }
  };
  return (
    <React.Fragment>
      {detail ? (
        <div className="wrapUnit">
          <table width="100%">
            <tbody>
              <tr>
                <td>{getTexto("Subtotal")}:</td>
                <td style={{ textAlign: "center" }}>
                  $ {subtotal} {getCurrency()}
                </td>
              </tr>
              <tr>
                <td>{getTexto("Quantity")}:</td>
                <td>
                  <div className="unit">
                    {renderInput}
                    <small
                      style={{
                        display: "block",
                        textAlign: "center",
                        color: "rgba(255, 68, 56, 0.9)",
                      }}
                    >
                      {error
                        ? `${getTexto(
                            "Minimum purchase of"
                          )} ${initialSell} ${getTexto(
                            "items or in groups of"
                          )} ${initialSell}`
                        : ""}
                    </small>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <div className="actions">
            <a onClick={addToCart.bind(this)} className="btn addtocart" href="">
              {getTexto("Add to Cart")}
            </a>
            <a className="btn saveforlater" href="">
              {getTexto("Save for Later")}
            </a>
          </div>
        </div>
      ) : cartItem ? (
        <React.Fragment>
          <div className="unit">
            {renderInput}
            <small
              style={{
                display: "block",
                textAlign: "center",
                color: "rgba(255, 68, 56, 0.9)",
              }}
            >
              {error
                ? `${getTexto("Minimum purchase of")} ${initialSell} ${getTexto(
                    "items or in groups of"
                  )} ${initialSell}`
                : ""}
            </small>
          </div>
          <div className="total" style={{ flexDirection: "column" }}>
            $ {parseFloat(priceInfOffer.price * unit).toFixed(2)}{" "}
            {getCurrency()}
            <small style={{ color: "rgb(255, 68, 56)" }}>
              $ {parseFloat(priceInfOffer.priceWDiscount * unit).toFixed(2)}{" "}
              {getCurrency()}
            </small>
          </div>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div className="unit">
            {renderInput}

            <small
              style={{
                display: "block",
                textAlign: "center",
                color: "rgba(255, 68, 56, 0.9)",
              }}
            >
              {error
                ? `${getTexto("Minimum purchase of")} ${initialSell} ${getTexto(
                    "items or in groups of"
                  )} ${initialSell}`
                : ""}
            </small>
          </div>
          {!error ? (
            <div className="action">
              <a onClick={addToCart.bind(this)} href="" className="btnAdd">
                {getTexto("Add to cart")}
              </a>
            </div>
          ) : (
            ""
          )}
        </React.Fragment>
      )}
      {notificationAdd == true ? (
        <Suspense fallback={"Loading"}>
          <Notification
            isUpdate={notificationUpdate}
            item={item}
            toClose={setNotification}
          />
        </Suspense>
      ) : (
        ""
      )}
    </React.Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    cart: state.cart,
    site: state.site,
  };
};
export default connect(mapStateToProps, { fetchCartItems, setItemToCart })(
  HandlerItem
);
