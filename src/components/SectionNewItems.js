import React from "react";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Item from "./Item";
import GenericItem from "./GenericItemInList";
import ShoppingHistory from "./user/shopping";
import Recommendations from "./user/recommendations";
import { Link } from "react-router-dom";
import { openModalForLogin, openModalForRetrieveUser } from "../libs/helpers";
import UserSection from "../components/login/UserSection";
import { Carousel } from "react-responsive-carousel";

export default function Sections({
  site,
  cart,
  dataSite = null,
  state,
  toLoginUser,
}) {
  console.log(state);
  let openModalLogin = (e) => {
    e.preventDefault();
    openModalForLogin(site.initialConfig, toLoginUser);
  };
  let openModalRetrieveUser = (e) => {
    e.preventDefault();
    openModalForRetrieveUser(site.initialConfig, toLoginUser);
  };
  return (
    <div className={`main_container`}>
      <div className={`list grid row sections`}>
        <div className="section" style={{ position: "relative" }}>
          <div className="section_content">
            <Carousel
              showArrows={true}
              showIndicators={true}
              showStatus={false}
              showThumbs={false}
              interval={10000}
              autoPlay={false}
              infiniteLoop={true}
            >
              <UserSection
                site={site}
                user={site.user}
                cart={cart}
                toLoginUser
              />

              <Recommendations state={state} site={site} cart={cart} />
              <ShoppingHistory state={state} site={site} cart={cart} />
              <div
                className="img-link"
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  padding: "0px 3px",
                  boxSizing: "border-box",
                }}
              >
                <a
                  style={{ width: "100%", textAlign: "center" }}
                  href={dataSite.contenido.imgBelowSlide.url}
                >
                  <picture loading="lazy">
                    {dataSite.contenido.imgBelowSlide.imgM != "" ? (
                      <source
                        media="(max-width: 767px)"
                        srcSet={`${dataSite.contenido.imgBelowSlide.imgM}`}
                      />
                    ) : (
                      ""
                    )}
                    <source
                      media="(min-width: 767px)"
                      srcSet={`${dataSite.contenido.imgBelowSlide.img}`}
                    />
                    <LazyLoadImage
                      className="img"
                      src={dataSite.contenido.imgBelowSlide.img}
                      alt="Image Below Slider"
                    />
                  </picture>
                </a>
              </div>
            </Carousel>
          </div>
        </div>
        <GenericItem>
          {console.log(dataSite.contenido)}
          <div className="section-offer">
            <Link to={dataSite.contenido.imgOffer.url}>
              <img src={dataSite.contenido.imgOffer.img} alt="Offer Image" />
            </Link>
          </div>
        </GenericItem>
        <Item key={state.storeItems[0].SItemCode} item={state.storeItems[0]} />
        <Item key={state.storeItems[1].SItemCode} item={state.storeItems[1]} />
        {/* <Item key={state.storeItems[2].SItemCode} item={state.storeItems[2]} /> */}
      </div>
    </div>
  );
}
