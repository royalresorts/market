import { Link } from "react-router-dom";
import getTexto from "../../libs/messages";
import React from "react";
import {
  openModalForLogin,
  openModalForRetrieveUser,
} from "../../libs/helpers";

const UserSection = ({
  site = null,
  user = null,
  cart = null,
  toLoginUser,
}) => {
  let openModalLogin = (e) => {
    e.preventDefault();
    openModalForLogin(site.initialConfig, toLoginUser);
  };
  let openModalRetrieveUser = (e) => {
    e.preventDefault();
    openModalForRetrieveUser(site.initialConfig, toLoginUser);
  };
  if (user) {
    return (
      <div className="section">
        <div className="section_content">
          <div className="avatar_information">
            <img
              width="80"
              height="80"
              src="/img/icons/avatar_anonimus.png"
              alt="Image Avatar"
            />
            <span>
              {getTexto("Hi")}, <br />
              {user.fullName.toLowerCase()}
            </span>
          </div>
          <div className="cart_information">
            <span>
              {getTexto("You have")} <strong>{cart && cart.itemsCount}</strong>{" "}
              {getTexto("items in your shopping cart")}
            </span>
            <img
              width="86"
              height="80"
              src="img/icons/cart_icon_2.png"
              alt="Cart Icon "
            />
          </div>
          <div className="action_link">
            <Link to={getURL("/cart-items")}>{getTexto("Cart")}</Link>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="section">
        <div className="section_content noLogged">
          <div className="avatar_information noLogged">
            <img
              width="50"
              height="50"
              src="/img/hand_hi.png"
              alt="Hand Image"
            />
            <span>
              {getTexto("Hi")} <br />
            </span>
          </div>
          <div className="cart_information" style={{ flex: "none" }}>
            <span>
              {getTexto("In order to give you a better experience please")}
            </span>
            <a className="btn naranja" onClick={openModalLogin}>
              {getTexto("log In")}
            </a>
            <small>{getTexto("(with your member account)")}</small>
          </div>
          <div className="action_link">
            {getTexto("or")}{" "}
            <a onClick={openModalRetrieveUser} href="#">
              {getTexto("Retrieve it here")}
            </a>
          </div>
        </div>
      </div>
    );
  }
};

export default UserSection;
