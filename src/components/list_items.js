import React from "react";
import Item from "./Item";
import ItemOffer from "./Item_offer";
import PageNavigation from "./pageNavigation";
import NoItems from "./list_items_no_items";
import { Redirect, useHistory, Link } from "react-router-dom";

export default function ListItems({
  category = null,
  search = null,
  params,
  state,
}) {
  let history = useHistory();
  let filterByCategory = "";
  let countRows = Math.trunc(6 / 4);
  let rowAdd = 6 % 4 > 0 ? 1 : 0;
  countRows = parseInt(countRows) + rowAdd;
  let items = state.storeItems;
  if (params.category && category) {
    if (params.category === "OFFER") {
      items = state.storeOffers;
    } else {
      items = state.storeItems.map((item) => {
        if (params.category === item.Category.SCategoryCode) {
          if (params.subcategory) {
            if (params.subcategory === item.ItemExt.Group.SGroupCode) {
              return item;
            }
          } else {
            return item;
          }
        }
      });
    }
  }

  // checking if we are in home page to avoid the 3 firsts items
  if (history.location.pathname === "/")
    items = items.filter((i, id) => i != undefined && id > 1);
  else items = items.filter((i, id) => i != undefined);

  let renderItems = [];
  items.forEach((item) => {
    if (item.OfferCode)
      renderItems.push(<ItemOffer key={item.ID} item={item} />);
    else renderItems.push(<Item key={item.SItemCode} item={item} />);
  });
  return (
    <div className={category ? "wrapperItemsOnCategories" : "main_container"}>
      <div
        style={{ gridTemplateRows: `auto repeat(${countRows},auto)` }}
        className="list grid"
      >
        {renderItems.length > 0 ? PageNavigation(renderItems) : <NoItems />}
      </div>
    </div>
  );
}
