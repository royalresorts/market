import React from "react";
const GenericItem = ({ children }) => {
  return (
    <div className="section" style={{ position: "relative" }}>
      <div className="section_content genericSection">{children}</div>
    </div>
  );
};

export default GenericItem;
