import React, { useState } from "react";
import getTexto from "../libs/messages";
import UnitHandler from "./unitHandler_offer";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Carousel } from "react-responsive-carousel";
import { Link } from "react-router-dom";
import { removeCharactersFromText } from "../libs/helpers";

export default function ItemOffer({ item }) {
  // const [value, setValue] = useState("");
  // const [number, setNumber] = useState(0);
  // const [errorMinSale, setError] = useState("");

  // window.scrollTo({
  //   top: 0,
  //   behavior: "smooth",
  // });
  // if (item === undefined) {
  //   return <div style={{ backgroundColor: "white" }}>no hay</div>;
  // }

  const getPrice = item.OfferDetail.reduce(
    (a, b) => a.Price - a.Discount + b.Price - b.Discount
  );

  const renderGalleryItem = () => {
    let itemsInGallery = [];
    if (item && item.ItemPhoto) {
      item.ItemPhoto.forEach((photo, id) =>
        itemsInGallery.push(
          <LazyLoadImage
            key={id}
            alt={item.OfferCode ? item.OfferCode : ""}
            src={photo.ItemPhoto ? photo.ItemPhoto : "/img/default.jpg"} // use normal <img> attributes as props
          />
        )
      );
    } else {
      itemsInGallery.push(
        <LazyLoadImage
          key={1}
          alt={item.OfferCode ? item.OfferCode : ""}
          src={"/img/default.jpg"} // use normal <img> attributes as props
        />
      );
    }
    return itemsInGallery;
  };

  return (
    <div className={`item `}>
      <div className="item_content">
        <div className={`image`}>
          <Carousel
            showArrows={true}
            showIndicators={true}
            showStatus={false}
            showThumbs={false}
            interval={5000}
            autoPlay={true}
            infiniteLoop={true}
          >
            {renderGalleryItem()}
          </Carousel>
        </div>

        <div className="description">
          <span style={{ textTransform: "capitalize" }}>
            {removeCharactersFromText(item.Description.toLowerCase())}
          </span>{" "}
          <br />
          <span>{item.OfferDetail.length + " " + getTexto("Items")}</span>{" "}
          <br />
          <strong>$ {parseFloat(getPrice).toFixed(2)} MXN</strong>
        </div>

        <UnitHandler isRelated={null} item={item} />
        <div className="details">
          <Link to={`/products/offer/${item.ID}`}>
            {getTexto("View details")}
          </Link>
        </div>
      </div>
    </div>
  );
}
