import React from "react";
import getTexto from "../libs/messages";
import UnitHandler from "./unitHandler_offer";
import { Carousel } from "react-responsive-carousel";
import { LazyLoadImage } from "react-lazy-load-image-component";

const ItemCart = ({ data, deleteItem }) => {
  let item = data.item;

  let deleteItemFunction = (itemCode) => {
    deleteItem(itemCode);
  };
  const renderGalleryItem = () => {
    let itemsInGallery = [];
    if (item && item.ItemPhoto) {
      item.ItemPhoto.forEach((photo, id) =>
        itemsInGallery.push(
          <LazyLoadImage
            key={id}
            alt={item.OfferCode ? item.OfferCode : ""}
            src={photo.ItemPhoto ? photo.ItemPhoto : "/img/default.jpg"} // use normal <img> attributes as props
          />
        )
      );
    } else {
      itemsInGallery.push(
        <LazyLoadImage
          key={1}
          alt={item.OfferCode ? item.OfferCode : ""}
          src={"/img/default.jpg"} // use normal <img> attributes as props
        />
      );
    }
    return itemsInGallery;
  };

  // return "SSS";
  return (
    <div className="itemCart">
      <div className="itemCart_container">
        <div className="itemCart_image">
          <Carousel
            showArrows={true}
            showIndicators={true}
            showStatus={false}
            showThumbs={false}
            interval={5000}
            autoPlay={true}
            infiniteLoop={true}
          >
            {renderGalleryItem()}
          </Carousel>
        </div>
        <div className="itemCart_description">
          <p style={{ textTransform: "capitalize" }}>
            {item.Description.toLowerCase()}{" "}
            <small>({`${item.OfferDetail.length} ${getTexto("Items")}`})</small>
          </p>
          <div className="itemCart_description_actions">
            <small>
              {getTexto("Unit price")}: $ {parseFloat(item.DPrice).toFixed(2)}{" "}
              MXN
            </small>
            <a
              onClick={(e) => {
                e.preventDefault();
                deleteItemFunction(item.SItemCode);
              }}
              href="#"
            >
              {getTexto("Delete")}
            </a>
          </div>
        </div>
        <UnitHandler cartItem={data} item={item} />
      </div>
    </div>
  );
};

export default ItemCart;
